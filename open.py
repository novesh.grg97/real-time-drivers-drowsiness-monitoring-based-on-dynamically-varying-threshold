import os
from scipy.spatial import distance as dist
from imutils import face_utils
from threading import Thread
import numpy as np
import playsound
import argparse
import imutils
import time
import dlib
import cv2
import sys
MOUTH_AR_THRESH = 0.1	
EYE_AR_THRESH = 0.15
global CONSECUTIVE_FRAMESEYES
CONSECUTIVE_FRAMESEYES=1
global CONSECUTIVE_FRAMESMOUTH
CONSECUTIVE_FRAMESMOUTH=1
global timeadd
timeadd=0

global COUNTEREYE
COUNTEREYE = 0
global COUNTERMOUTH
COUNTERMOUTH = 0
global millis
global nn
nn=3
global cf
cf=0
global ALARM_ON
ALARM_ON = False

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")




(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]
(mStart, mEnd) = face_utils.FACIAL_LANDMARKS_IDXS["mouth"]
print("Predicting images...")



def sound_alarm(path):
	playsound.playsound(path)

def eye_aspect_ratio(eye):
	A = dist.euclidean(eye[1], eye[5])
	B = dist.euclidean(eye[2], eye[4])
	C = dist.euclidean(eye[0], eye[3])
	ear = (A + B) / (2.0 * C)

	return ear
def mouth_aspect_ratio(mouth):

	A = dist.euclidean(mouth[13], mouth[19])
	B = dist.euclidean(mouth[15], mouth[17])

	
	C = dist.euclidean(mouth[12], mouth[16])

	mar = (A + B) / (2.0 * C)
	return mar

def detect_face(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier('opencv-files/lbpcascade_frontalface.xml')

    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5);

    if (len(faces) == 0):
        return None, None

    (x, y, w, h) = faces[0]

    return gray[y:y+w, x:x+h], faces[0]

def unknown(video):
    print("INSIDE UNKNOWN")
    global cf
    global nn
    global millis
    
    millis=0
    nn+=1
    print(nn)
    name=[]
    faceu=[]
    i=0
    cf=0
    while i<30:
        ret,frame=video.read()
        face,rect=detect_face(frame)
        if face is None:
                continue
        
 
        cv2.waitKey(1)
        
        if face is not None:
            faceu.append(face)
            name.append(nn)
            face_recognizer.train(faceu,np.array(name))
        i+=1
        millis = int(round(time.time()))
        



def prepare_training_data(data_folder_path):

    dirs = os.listdir(data_folder_path)

    faces = []
    labels = []
    for dir_name in dirs:

        if not dir_name.startswith("s"):
            continue;
        label = int(dir_name.replace("s", ""))
        subject_dir_path = data_folder_path + "/" + dir_name
        subject_images_names = os.listdir(subject_dir_path)

        for image_name in subject_images_names:

            if image_name.startswith("."):
                continue;

            image_path = subject_dir_path + "/" + image_name

            image = cv2.imread(image_path)

            #cv2.imshow("Training on image...", cv2.resize(image, (400, 500)))
            cv2.waitKey(100)

            face, rect = detect_face(image)

            if face is not None:
                faces.append(face)
                labels.append(label)

    cv2.destroyAllWindows()
    cv2.waitKey(1)
    cv2.destroyAllWindows()

    return faces, labels
i=1
img=cv2.VideoCapture(0)
while i<21:
    a='training-data/s3/'+str(i)+'.jpg'
    ret,frame=img.read()
    cv2.imwrite(a,frame)
    i=i+1
img.release()
cv2.destroyAllWindows()
print("Training data...")
faces, labels = prepare_training_data("training-data")
print("Data prepared")

print("Total faces: ", len(faces))
print("Total labels: ", len(labels))


face_recognizer = cv2.face.LBPHFaceRecognizer_create()


face_recognizer.train(faces, np.array(labels))



def draw_rectangle(img, rect):
    (x, y, w, h) = rect
    cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)

def draw_text(img, text, x, y):
    cv2.putText(img, text, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 255, 0), 2)



def predict(test_img,abc):
    global ALARM_ON
    global COUNTEREYE
    global timeadd
    global COUNTERMOUTH
    global cf
    global CONSECUTIVE_FRAMESEYES
    global CONSECUTIVE_FRAMESMOUTH
    img = test_img.copy()
    face, rectangle = detect_face(img)
    if face is None:
        return img
 
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	
    rects = detector(gray, 0)
	
    for rect in rects:
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        print("INSIDE")
        leftEye = shape[lStart:lEnd]
        rightEye = shape[rStart:rEnd]
        mouth = shape[mStart:mEnd]
        lefEAR = eye_aspect_ratio(leftEye)
        rightEAR = eye_aspect_ratio(rightEye)
        mouthEAR = mouth_aspect_ratio(mouth)
        ear = (lefEAR + rightEAR) / 2.0
        leftEyeHull = cv2.convexHull(leftEye)
        rightEyeHull = cv2.convexHull(rightEye)
        mouthHull = cv2.convexHull(mouth)


            
        millis1 = int(round(time.time()))
        millis1=millis1+timeadd
        if millis1-millis == 43200:
                cv2.putText(img, "STOP DRIVING!", (10, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        if millis1-millis > 43201:     
                sys.exit(0)
                cv2.destroyAllWindows()
        cv2.putText(img, "TIME: 42801", (350, 90),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (94, 181, 255), 2)
        cv2.drawContours(img, [leftEyeHull], -1, (0, 255, 0), 1)
        cv2.drawContours(img, [rightEyeHull], -1, (0, 255, 0), 1)
        cv2.drawContours(img, [mouthHull], -1, (0,255,0), 1)
        if (mouthEAR > MOUTH_AR_THRESH):
            COUNTERMOUTH += 1
            if millis1-millis > 10800:
                    CONSECUTIVE_FRAMESMOUTH=90-2.72*((millis1-millis)/3600)
            
            if COUNTERMOUTH >= CONSECUTIVE_FRAMESMOUTH:
##                if not ALARM_ON:
##                    ALARM_ON = True
##                    t = Thread(target=sound_alarm("alarm.wav"))
##                    t.deamon = True
##                    t.start()
                cv2.putText(img, "DROWSINESS ALERT!", (10, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        else:
            COUNTERMOUTH = 0
            ALARM_ON = False

            
        if (ear < EYE_AR_THRESH):
            COUNTEREYE += 1
            if millis1-millis > 10800:
                    CONSECUTIVE_FRAMESEYES=50-1.81*((millis1-millis)/3600)
            if COUNTEREYE >= CONSECUTIVE_FRAMESEYES:
                cv2.putText(img, "DROWSINESS ALERT!", (10, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
                    
        else:
            COUNTEREYE = 0
            ALARM_ON = False
            
        cv2.putText(img, "EAR: {:.2f}".format(ear), (350, 130),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        cv2.putText(img, "MAR: {:.2f}".format(mouthEAR), (350, 150),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            
        cv2.putText(img, "CM: {:2f}".format(CONSECUTIVE_FRAMESMOUTH),(350, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (94, 181, 255), 2)
        cv2.putText(img, "CE: {:2f}".format(CONSECUTIVE_FRAMESEYES), (350, 50),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (94, 181, 255), 2)
        cv2.putText(img, "CF: {:2f}".format(cf), (350, 70),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (94, 181, 255), 2)
        (x,y,w,h)=face_utils.rect_to_bb(rect)
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
## 
        for (x,y) in shape:
            cv2.circle(img, (x, y), 1, (0, 0, 255), -1)
    
        label, confidence = face_recognizer.predict(face)

        if(confidence>75):
##            print("inside 1")
            cf+=1
##            print(cf)
            if(cf>150000):
##                print("inside2")
                unknown(abc)
        else:
            cf=0  
   
##    draw_rectangle(img, rectangle)
        confidence=int(confidence)
        label=int(label)
     
## 
##        draw_text(img, label, rectangle[0], rectangle[1])
       draw_text(img, confidence, rectangle[0]+40, rectangle[1])
##

    return img

abc=cv2.VideoCapture("merged.mp4")
cv2.waitKey(0)
millis = int(round(time.time()))
flag=0
while True:
    ret,frame=abc.read()
    frame = imutils.resize(frame, width=450)
##    frame = imutils.rotate(frame, -90)
    predicted_img1= predict(frame,abc)

    cv2.imshow("OUTPUT", predicted_img1)
    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        break
cv2.destroyAllWindows()
abc.release()


